import java.util.Scanner;
import java.io.*;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.ObjectInputStream;
//import java.io.FileInputStream;
//import java.io.ObjectOutputStream;
//import java.io.FileOutputStream;

public class Buscaminas {
   static Scanner sc = new Scanner(System.in);
   public static void main (String[] args) throws Exception{
      int opt;
      boolean exitGame;
   
      exitGame = false;
      do {
         printMainMenu();
         do {
            opt = readOption();
         } while(opt == -1);
      
         switch(opt) {
            case 'C':
               newGame();
               break;
            case 'D':
               String partida = selectGame();
               loadGame(partida);
               break;
            case 'P':
               PrintScore();
               break;
            case 'Q':
               exitGame = true;
               break;
            default:
               System.out.println("\n\n#### OPCION INCORRECTA ####\n\n");
         }
      } while(!exitGame);
   }

   private static String selectGame() {
      String[] files = new File("./savedGames").list();
      String game = new String();
      int optNum;
      String opt;
      boolean exit, validGame;
   
      System.out.println("\n\n\n ### PARTIDAS GUARDADAS ### \n\n");
   
      for(int i = 0; i < files.length; i++) {
         System.out.printf("%n%d- %s%n", i, files[i]);
      }
      System.out.println();
   
      validGame = exit = false;
      do {
         System.out.print("Escoja una partida: "); 
         opt = sc.nextLine().trim().toUpperCase();
         if(!opt.equals("Q")){
            if(opt.matches("[0-9]+")){
               optNum = Integer.parseInt(opt);
               if(optNum >= 0 && optNum < files.length){ 
                  game = "./savedGames/" + files[optNum];
                  validGame = true;
               }
               else{
                  System.out.println("\n\n### LA PARTIDA NO EXISTE ### \n\n"); 
               }
            } 
            else {
               System.out.println("\n\n### INTRODUZCA EL NUMERO DE LA PARTIDA DESEADA ### \n\n");
            }
         }
         else {
            exit = true;
         } 
      }while(!exit && !validGame); 
   
      return game;
   }


   private static void newGame() {
      boolean[][] minesBoard;
      int[][] playerBoard;
      boolean badOpt = false;
      int score = 0;
      int opt;
      do {
         printNewGameMenu();
         opt = readOption();
         switch(opt) {
            case 'Q':
               badOpt = false;
               break;
            case 'A':
               int data[];
               do {
                  System.out.print("Introduce, separados por espacios, el numero de minas, las filas y las columnas:  ");
                  data = getMinesRowsCols();
                  if(data == null){
                     System.out.println("\n\n ### DATOS INCORRECTOS ### \n\n");
                  }
               } while(data == null);
            
               minesBoard = getMinesBoard(data[0], data[1], data[2]);
               playerBoard = new int[data[1]][data[2]];
            
               configGame(minesBoard, playerBoard, score);
               break;
            case 'I':
               break;
            default:
               badOpt = true;
         }
      } while(badOpt);
   }

   private static void configGame(boolean[][] minesBoard, int[][] playerBoard, int score) {
      boolean badOpt, exit = false;
      int opt;
   
      do {
         badOpt = false;
         printConfigGameMenu();
         opt = readOption();
      
         switch (opt) {
            case 'Q':
               exit = true;
               break;
            case 'V':
               showMinesBoard(minesBoard);
               break;
            case 'D':
               saveGame(minesBoard, playerBoard, score);
               break;
            case 'E':
               break;
            case 'J':
               score = play(minesBoard, playerBoard, score);
               if(score < 0){
                  exit = true;
               }
               break;
            default:
               badOpt = true;
         }
      } while(!exit || badOpt);
   
   }
   private static void SaveScore(int score){      
      try { 
         FileWriter fw   = new  FileWriter("puntuacions.txt",true);
         BufferedWriter bw = new BufferedWriter(fw);
         System.out.print("Nombre de jugador: "); 
         String nombre = sc.nextLine();         
         String cadena = String.format("%-15s%s",String.valueOf(score),nombre);
         bw.write(cadena);
         bw.newLine();
         bw.close();
      } 
      catch(Exception e){ 
           System.out.println(e); 
      }
   }

   private static void PrintScore(){      
      try 
      { 
         FileReader fr = new FileReader("puntuacions.txt");
         BufferedReader entrada = new BufferedReader(fr);
         String score;
         System.out.printf("%n%nPuntuaciones:%n%n");
         while((score = entrada.readLine()) != null) { 
            System.out.println(score);
         }
         System.out.printf("%n%n");
         entrada.close(); 
      } 
      catch(java.io.FileNotFoundException fnfex) { 
         System.out.println("Archivo no encontrado: " + fnfex); 
      } 
      catch(java.io.IOException ioex) {}
   }
   private static boolean gameExist(String game) {
      boolean exist = false;
      String[] files = new File("./savedGames").list();
   
      for(int i = 0; i < files.length && !exist; i++) {
         if(files[i].equals(game)){
            exist = true;
         }
      } 
   
      return exist;
   }
   
   private static void saveGameName(String gameName) { 
      try { 
         FileWriter fw = new FileWriter("./savedGames/savedGamesList"); 
         fw.write(gameName);
      }
      catch(Exception e) {
         System.out.println(e);
      }
   }

   private static void saveGame(boolean[][] minesBoard, int[][] playerBoard, int score) {
      String gameName;
      do {
         System.out.print("Introduzca el nombre de la partida: "); 
         gameName = sc.nextLine();
      }
       while(gameExist(gameName)); 
       
      try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("./savedGames/" + gameName))) {
         oos.writeObject(minesBoard);
         oos.writeObject(playerBoard);
         oos.writeInt(score);
         System.out.println("\n\n ### LA PARTIDA HA SIDO GUARDADA ### \n\n");
      
      } 
      catch (IOException io) {
         System.out.println("\n\n ### ERROR DE ENTRADA/SALIDA ### \n\n");
      } 
      catch (Exception e) {
         System.out.println(e);
      }
   }

   private static int[][] genAdyMinesBoard(boolean[][] minesBoard) {
      int[][] adyMines = new int[minesBoard.length][minesBoard[0].length];
   
      int rows = minesBoard.length;
      int cols = minesBoard[0].length;
   
      for(int i = 0; i < rows; i++ ) {
         for(int j = 0; j < cols; j++) {
            int[][] adyacents = getAdjacents(i, j);
            for(int k = 0; k < adyacents.length; k++) {
               int r = adyacents[k][0];
               int c = adyacents[k][1];
               if(r >= 0 && r < rows && c >= 0 && c < cols){
                  if(minesBoard[r][c]){
                     adyMines[i][j]++;
                  }
               }
            }
         }
      }
   
      return adyMines;
   }

   private static int play(boolean[][] minesBoard, int[][] playerBoard,int score) {
      char action;
      long startTime = System.currentTimeMillis();
      int initScore = score;
      String input;
      boolean exit, boom, winner;
      int[][] adyMinesBoard = genAdyMinesBoard(minesBoard);
      int[] coords = new int[2];
      int rows, cols;
      int mines = countMines(minesBoard);
      int totalMines = mines;
      
      rows = minesBoard.length;
      cols = minesBoard[0].length;
      exit = boom = winner = false;
      
      Scanner in = new Scanner(System.in);
      do {
         score = (int)(initScore + (System.currentTimeMillis() -  startTime)/1000);
         showPlayerBoard(playerBoard, adyMinesBoard, score, mines);
         System.out.print("Introdueix una opció: ");
         input = in.nextLine().trim();
      
         if(input.matches("[Qq]")){
            exit = true;
         }
         else if(input.matches("[A-Za-z]\\s+\\d+\\s+\\d+")){
            action = getAction(input, coords);
            if(coords[0] < rows && coords[1] < cols) {
               switch(action) {
                  case 'M':
                     if(toogleFlag(playerBoard, coords)){
                        --mines;
                     }
                     else{
                        ++mines;
                     }
                     break;
                  case 'D':
                     boom = uncover(minesBoard, playerBoard,adyMinesBoard, coords);
               }
               winner = checkWinner(playerBoard, totalMines);
            }
         }
      } while(!exit && !boom && !winner);
      if(boom || winner){
         int newScore = score;         
         if(boom) {
            showResumeBoard(minesBoard, playerBoard, score);
            System.out.println("\n\n ### YOU FAIL ###  \n\n");
            score = -1;            
         }   
         else{
            showPlayerBoard(playerBoard, adyMinesBoard, score, mines);
            System.out.println("\n\n ### YOU WIN ### \n\n");
            score = -2;
         }
         SaveScore(newScore);
      }
   
      return score;
   }

   private static boolean checkWinner(int[][] playerBoard, int totalMines) {
      int win = 0;
      int total = 0;
      for(int i = 0; i < playerBoard.length; i++) {
         for(int j = 0; j < playerBoard[i].length; j++) {
            if(playerBoard[i][j] != 1){
               win++;
            }
            total++;
         }         
      }   
      return (win) == total - totalMines;
   }

   private static boolean toogleFlag(int[][] playerBoard, int[] coords) {
      int r = coords[0], c = coords[1];
      boolean flag = false;
      int n = playerBoard[r][c];
   
      if(n == 2){
         n = 0;
      }
      else if(n == 0){
         n = 2;
         flag = true;
      }
   
      playerBoard[r][c] = n;
   
      return flag;
   }

   private static char getAction(String input,int[] coords) {
      String[] fields;
      fields = input.split("\\s+");
      coords[0] = Integer.parseInt(fields[1]);
      coords[1] = Integer.parseInt(fields[2]);
   
      return Character.toUpperCase(input.charAt(0));
   }
   
   private static char[][] toCharAdyBoard(int[][] adyMines){
      char[][] charMinesBoard = new char[adyMines.length][adyMines[0].length];
      for(int i = 0; i < charMinesBoard.length; i++){
         for(int j = 0; j < charMinesBoard[i].length; j++){
            charMinesBoard[i][j] = (char)(adyMines[i][j] + '0');; 
         }
      }
      return charMinesBoard;
   }
   
   private static void showAdyBoard(int[][] adyMines) {
      printBoard(adyMines.length, adyMines[0].length, toCharAdyBoard(adyMines));
   }
   
   private static void printMainMenu() {
      System.out.print(
             "Benvinguts al joc CERCA MINES \n\n" +
                     "C. Començar partida nova\n" +
                     "D. Continuar partida desada\n" +
                     "P. Veure les puntuacions\n" +
                     "Q. Sortir\n\n" +
                     "Introdueix una opcio: "
         );
   }


   private static void printNewGameMenu() {
      System.out.print(
             "CERCA MINES: NOVA PARTIDA\n\n" +
                     "Q. Tornar al menu principal\n" +
                     "A. Generar el taulell de mines aleatoriament\n" +
                     "I. Importar un taulell de mines des d'un fitxer\n\n" +
                     "Introdueix una opcio: "
         );
   }
   private static void printConfigGameMenu() {
      System.out.print(
             "CERCA MINES: CONFIGURACIO PARTIDA\n\n" +
                     "Q. Tornar al menu principal\n" +
                     "V. Veure el taulell de mines\n" +
                     "D. Desar la partida en l'estat actual\n" +
                     "E. Netejar el taulell del jugador\n" +
                     "J. Jugar\n\n"
         );
   }

   private static boolean [][] getMinesBoard(int mines, int rows, int cols) {
      boolean[][] minesBoard = new boolean[rows][cols];
      int[] coordMine;
      int[] lastCoordMine = {-1, -1};
      int cont = 10;
   
      for(int i = 0; i < mines; i++) {
         do{
            coordMine = genCoordMine(rows, cols);
         } while(minesBoard[coordMine[0]][coordMine[1]] == true);
      
         minesBoard[coordMine[0]][coordMine[1]] = true;
      }
   
      return minesBoard;
   }

   private static int[] genCoordMine(int x, int y) {
      int row, col;
      int[] coordMine = new int[2];
      row = (int)(Math.random() * x);
      col = (int)(Math.random() * y);
   
      coordMine[0] = row;
      coordMine[1] = col;
   
      return coordMine;
   }

   private static int[] getMinesRowsCols() {
      int[] data = null;
      Scanner in = new Scanner(System.in);
      String line = in.nextLine().trim();
      int mines, rows, cols;
   
      if(line.matches("\\d+\\s+\\d+\\s+\\d+")) {
         data = parseIntArray(line.split("\\s+"));
         mines = data[0]; rows = data[1]; cols = data[2];
         if( mines == 0 || rows == 0 || cols == 0 || mines >= rows*cols){
            data = null;
         }
      }
   
      return data;
   }

   private static int[] parseIntArray(String[] vs) {
      int[] parsed = new int[vs.length];
   
      for(int i = 0; i < vs.length; i++) {
         parsed[i] = Integer.parseInt(vs[i]);
      }
   
      return parsed;
   }


   private static int readOption() {
      int opt = -1;
      Scanner in = new Scanner(System.in);
      String line = in.nextLine().trim();
      if(line.matches("[A-Za-z]")){
         opt = Character.toUpperCase(line.charAt(0));
      }
      return opt;
   }
   
   private static char[][] toCharMinesBoard(boolean[][] mines){
      char[][] charMinesBoard = new char[mines.length][mines[0].length];
      for(int i = 0; i < charMinesBoard.length; i++){
         for(int j = 0; j < charMinesBoard[i].length; j++){
            char ch;
            if(mines[i][j]){
               ch= '*';
            }
            else{
               ch= '-';
            }
            charMinesBoard[i][j] = ch; 
         }
      }
      return charMinesBoard;
   }

   private static void showMinesBoard(boolean[][] mines) {
   
      printBoard(mines.length, mines[0].length, toCharMinesBoard(mines));
   }
   
   private static char[][] toCharPlayerBoard(int[][] playerBoard,int[][] adyMinesBoard){
      char[][] charMinesBoard = new char[playerBoard.length][playerBoard[0].length];
      for(int i = 0; i < charMinesBoard.length; i++){
         for(int j = 0; j < charMinesBoard[i].length; j++){
            char ch;
            switch(playerBoard[i][j]) {
               case 0:
                  ch = '~';
                  break;
               case 1:
                  int n = adyMinesBoard[i][j];
                  if(n == 0){
                     ch = ' ';
                  }
                  else{
                     ch = (char)(n + '0');
                  }
                  break;
               default:
                  ch = 'b';
            }
         
            charMinesBoard[i][j] = ch; 
         }
      }
      return charMinesBoard;
   }
   private static void showPlayerBoard(int[][] playerBoard,int[][] adyMinesBoard, int score, int mines) {
      System.out.println("CERCA MINES. JOC");
      System.out.printf("PUNTUACIO: %d%n", score);
      System.out.printf("MINES: %d%n", mines);
   
      printBoard(playerBoard.length, playerBoard[0].length, toCharPlayerBoard(playerBoard,adyMinesBoard));
   }
   private static char[][] toCharResumeBoard(boolean[][] minesBoard,int[][] playerBoard){
      char[][] charMinesBoard = new char[playerBoard.length][playerBoard[0].length];
      for(int i = 0; i < charMinesBoard.length; i++){
         for(int j = 0; j < charMinesBoard[i].length; j++){
            boolean mine = minesBoard[i][j];
            int state = playerBoard[i][j];
            char ch;
                
            if(state == 1){
               if(mine){
                  ch = '#';
               }
               else{
                  ch = ' ';
               }
            }
            else if(state == 2){
               if(mine){
                  ch = '0';
               }
               else{
                  ch = 'x';
               }
            }
            else{
               if(mine){
                  ch = '*';
               }
               else{
                  ch = ' ';
               }
            }               
         
            charMinesBoard[i][j] = ch; 
         }
      }
      return charMinesBoard;
   }

   private static void showResumeBoard(boolean[][] minesBoard,int[][] playerBoard,int score) {   
      printBoard(playerBoard.length, playerBoard[0].length, toCharResumeBoard(minesBoard,playerBoard));
   }

   private static void printBoard(int rows, int cols, char[][] simbols) {
      char ch;
      int lineLength = cols*4 + 4;
      String lineSeparator = String.format("  %-" + (lineLength - 2) + "s|", "|");
      StringBuilder line;
      StringBuilder border = new StringBuilder(lineLength - 2);
   
      border.append("  ");
      int cont = 0;
      while(++cont <= lineLength - 1) {
         border.append("-");
      }
   
      System.out.println(border);
      System.out.println(lineSeparator);
   
      for (int i = 0; i < rows; i++) {
         line = new StringBuilder(lineLength);
         line.append(String.format("%2s%-2s", i, "|")) ;
         for(int j = 0; j < cols; j++) {
            ch = simbols[i][j];
            line.append(ch + "   ");
         }
      
         line.append("|");
         System.out.println(line);
         System.out.println(lineSeparator);
      }
   
      System.out.println(border);
   
      line = new StringBuilder(lineLength);
      line.append("    ");
      for(int k = 0; k < cols; k++){
         line.append(k + "   ");
      }
      System.out.println(line);
      System.out.println();
   }


   private static boolean uncover(boolean[][] minesBoard, int[][] playerBoard,int[][] adyMinesBoard, int[] coords) {
      int x = coords[0];
      int y = coords[1];
      int rows = playerBoard.length;
      int cols = playerBoard[0].length;
      int state;
      boolean boom = false;
   
      if(playerBoard[x][y] != 2) {
         boom = minesBoard[x][y];
         state = playerBoard[x][y];
      
         playerBoard[x][y] = 1;
         if(!boom){
            int[][] adyacents = getAdjacents(x, y);
            for(int i = 0; i < adyacents.length && !boom; i++) {
               if(isValidCoord(adyacents[i], rows, cols)) {
                  int r = adyacents[i][0];
                  int c = adyacents[i][1];
                  if(playerBoard[r][c] == 0) {
                     if(state == 1) {
                        int flagsAround = getFlagsAround(playerBoard, coords);
                        if(flagsAround == adyMinesBoard[x][y]) {
                           playerBoard[r][c] = 1;
                           boom = minesBoard[r][c];
                        }
                     }
                     else if(adyMinesBoard[x][y] == 0 ){
                        uncover(minesBoard, playerBoard,adyMinesBoard, adyacents[i]);
                     }
                  }
               }
            }
         }
      }
   
      return boom;
   }

   private static int getFlagsAround(int[][] playerBoard, int[] coords) {
      int rows = playerBoard.length;
      int cols = playerBoard[0].length;
      int flags = 0;
   
      int[][] adyacents = getAdjacents(coords[0], coords[1]);
      for(int i = 0; i < adyacents.length; i++) {
         if(isValidCoord(adyacents[i], rows, cols)) {
            int r = adyacents[i][0];
            int c = adyacents[i][1];
            if(playerBoard[r][c] == 2){
               flags++;
            }
         }
      }
   
      return flags;
   }

   private static int[][] getAdjacents(int x, int y) {
      int[][] adjacents = {{x - 1, y}, {x + 1, y}, {x, y - 1}, {x, y + 1}, {x+1, y-1}, {x+1, y+1}, {x-1, y-1}, {x-1, y+1} };
      return adjacents;
   }


   private static boolean isValidCoord(int[] coord, int rows, int cols) {
      int r = coord[0];
      int c = coord[1];
   
      boolean valid  = r >= 0 && r < rows && c >= 0 && c < cols;
   
      return valid;
   }

   private static void loadGame (String partida) {
      boolean[][] minesBoard;
      int[][] playerBoard;
      int score;
   
      try( ObjectInputStream ois = new ObjectInputStream(new FileInputStream(partida))) {
         minesBoard = (boolean[][]) ois.readObject();
         playerBoard = (int[][]) ois.readObject();
         score = ois.readInt();
      
         System.out.println("\n\n ### LA PARTIDA HA SIDO CARGADA ### \n\n");
         configGame(minesBoard, playerBoard, score);
      }
      catch(FileNotFoundException fnf) {
         System.out.println("\n\n ### NO HAY NINGUNA PARTIDA GUARDADA ### \n\n");
      }
      catch(Exception e){ System.out.println(e);}
   }

   private static int countMines(boolean[][] minesBoard) {
      int num = 0;
   
      for(int i = 0; i < minesBoard.length; i++) {
         for(int j = 0; j < minesBoard[i].length; j++) {
            if(minesBoard[i][j]) {
               num++ ;
            }
         }
      }
   
      return num;
   }
}
